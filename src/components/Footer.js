import React from 'react'
import { Box, Stack, Typography } from '@mui/material'

import Logo from '../assets/images/Logo-1.png'

const Footer = () => {
  return (
    <div style={{marginTop:'80px', backgroundColor:'#fff3f4', width:'100%'}}>
      <Stack gap='40px' alignItems='center' px='40px' pt='24px'>
        
        <Typography variant='h6' >
          Made with 💪 & ❤ by :
        </Typography>
        <img src={Logo} width='200px' height='40px' mt='20px' mb="40px" />
        
      </Stack>
    </div>
  )
}

export default Footer